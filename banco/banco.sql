Create database if not exists pizzaria;
use pizzaria;

create table cliente (
	id_cliente int primary key AUTO_INCREMENT,
	nome varchar(255),
	sobrenome varchar(255),
	telefone int,
	endereco varchar(255)
);

create table pedido (
	data datetime,
	estado varchar(30),
	PRECOTOTAL float,
	id_pedido int primary key AUTO_INCREMENT,
	id_cliente int,
	foreign key (id_cliente) references cliente (id_cliente)
);
create table forma (
	id_forma int primary key AUTO_INCREMENT,
	descricao varchar(30)
);

create table tipo (
	id_tipo int primary key AUTO_INCREMENT,
	nome_tipo varchar(20),
	preco_cm2 float
);

create table sabor (
	id_sabor int primary key AUTO_INCREMENT,
	nome varchar(30),
	descricao varchar(255),
	id_tipo int,
	foreign key (id_tipo) references tipo (id_tipo)
);

create table itens_pedido (
	id_item int primary key AUTO_INCREMENT,
	id_forma int,
    ID_SABOR INT,
	ID_SABOR2 INT,
	tamanho int,
	quantidade int,pedido
	observacao varchar(255),
	preco float,
	id_pedido int,
	foreign key (id_pedido) references pedido(id_pedido)
);

INSERT INTO FORMA(DESCRICAO) 
VALUES('Quadrado'),
	  ('Circulo'),
	  ('Triangulo');

INSERT INTO TIPO(NOME_TIPO,PRECO_CM2) 
VALUES('Especial',0.5),
	  ('Premium',1.0),
	  ('Normal',0.3);
