/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Telas;

import Pizzaria.*;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TelaFormSabor extends javax.swing.JFrame {
    private Sabor s;    
    private SaborDaoImpl saborDAO = new SaborDaoImpl();
    private List<Tipo> tipos;
    private JFrame telaAnterior;
    /*
     * Creates new form AlterarSabor
     */
    public TelaFormSabor(SaborDaoImpl SaborDAO, JFrame telaAnterior) {
        initComponents();
        this.telaAnterior = telaAnterior;
        this.saborDAO = (SaborDaoImpl) SaborDAO;
        buttonAcao.setText("Cadastrar");
        atualizaTipos();
    }
    
    public TelaFormSabor(SaborDaoImpl saborDAO,Sabor s, JFrame telaAnterior ) {
        initComponents();
        this.telaAnterior = telaAnterior;
        this.saborDAO = (SaborDaoImpl) saborDAO;
        this.s = s;
        nomeSabor.setText(s.getNome());
        descricaoSabor.setText(s.getDescricao());
        buttonAcao.setText("Atualizar");
        atualizaTipos();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nomeSabor = new javax.swing.JTextField();
        descricaoSabor = new javax.swing.JTextField();
        buttonAcao = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        comboTipo = new javax.swing.JComboBox<>();
        voltar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Sabor");

        jLabel2.setText("Nome:");

        jLabel3.setText("Descrição:");

        buttonAcao.setText("Acao");
        buttonAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAcaoActionPerformed(evt);
            }
        });

        jLabel4.setText("Tipo :");

        voltar.setText("Voltar");
        voltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(108, 108, 108)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel4)))
                        .addGap(0, 98, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(voltar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonAcao))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(nomeSabor)
                                    .addComponent(descricaoSabor))))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nomeSabor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(descricaoSabor, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonAcao)
                    .addComponent(voltar))
                .addGap(17, 17, 17))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAcaoActionPerformed
        String nome = nomeSabor.getText();
        String  descricao = descricaoSabor.getText();float p = 0;
        if(!nome.equals("") && comboTipo.getSelectedIndex() > -1 && saborDAO != null){
            if(buttonAcao.getText().equals("Cadastrar") && s == null){               
                if(saborDAO != null){
                    s = new Sabor();
                    s.setNome(nome);
                    s.setDescricao(descricao);
                    s.setTipo(tipos.get(comboTipo.getSelectedIndex()));
                    saborDAO.inserirSabor(s);
                }
            }
            else if(buttonAcao.getText().equals("Atualizar")){
                    s.setNome(nome);
                    s.setDescricao(descricao);
                    saborDAO.alterarSabor(s);
            }
            telaAnterior.setVisible(true);
            this.dispose();
        }
        else{
            JOptionPane.showMessageDialog(null,"Todos os campos devem ser preenchidos","Campos Faltando",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_buttonAcaoActionPerformed

    private void voltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voltarActionPerformed
        telaAnterior.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_voltarActionPerformed
    
    public void atualizaTipos(){
        TipoDAOImpl tDAO = new TipoDAOImpl();
        tipos = tDAO.buscaTodosTipo();
        for(Tipo t : tipos)
            comboTipo.addItem(t.getNome());
        
        tDAO.close();
    }
   
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAcao;
    private javax.swing.JComboBox<String> comboTipo;
    private javax.swing.JTextField descricaoSabor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField nomeSabor;
    private javax.swing.JButton voltar;
    // End of variables declaration//GEN-END:variables
}
