/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

public class Cliente {
    
    private int idCliente;
    private String nome;
    private String sobreNome;
    private String telefone;
    
    public Cliente(){
        
    }
    public Cliente(String nome, String sobreNome, String telefone){
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return nome + " " + sobreNome;
    }
    
}
