/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

public class Triangulo extends Forma{

    public Triangulo(){
        super.setIdForma(3);
        super.setDescricao("Triangulo");
        super.setValorMax(60);
        super.setValorMin(20);
    }
    
    public float area(int tamanho) {
        return (float) (Math.pow(tamanho,2) * Math.sqrt(3))/4;
    }
    public int tamanho(float area){
        return (int) Math.round((float) (area * 4)/Math.sqrt(3));
    }
    
}