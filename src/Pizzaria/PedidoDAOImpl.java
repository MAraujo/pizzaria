/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PedidoDAOImpl implements PedidoDAO{
    
    private Connection con;
    
    public PedidoDAOImpl(){
        
         try {
             this.con = ConnectionFactory.getConnection();
         } catch (ClassNotFoundException ex) {
             ex.printStackTrace();
         } catch (SQLException ex) {
             ex.printStackTrace();
         }
        
    }
    public PedidoDAOImpl(Connection con){
        this.con = con;
    }
    
    public boolean insertPedido(Pedido p) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("INSERT INTO PEDIDO(ID_CLIENTE,PRECOTOTAL,ESTADO,DATA) VALUES( ?,?,?,?);");
            st.setInt(1, p.getCliente().getIdCliente());
            st.setFloat(2, p.atualizaPrecoTotal());
            st.setString(3, p.getEstado());
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String agora = sdf.format(dt);
            st.setString(4, agora);
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                ItemPedidoDAOImpl itemPedidoDAO = new ItemPedidoDAOImpl(con);
                Pedido p2 = selectUltimoPedido();
                for(ItemPedido ip : p.getItensPedido()){
                    ip.setPedido(p2);
                    if(!itemPedidoDAO.insertItemPedido(ip)){
                        return false;
                    }
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean updatePedidoEstado(Pedido p) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("UPDATE PEDIDO P SET P.ESTADO = ? WHERE ID_PEDIDO = ?");
            st.setString(1, p.getEstado());
            st.setInt(2, p.getIdPedido());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    public boolean updatePedido(Pedido p) {
        PreparedStatement st = null;
        ItemPedidoDAOImpl ipDAO = new ItemPedidoDAOImpl();
        try {
            st = con.prepareStatement("UPDATE PEDIDO P SET  P.PRECOTOTAL = ?, P.DATA = ? WHERE ID_PEDIDO = ?");
            java.text.SimpleDateFormat sdf = 
            new SimpleDateFormat("yyyy-MM-dd");
            Date dt = new Date();
            String data = sdf.format(dt);
            p.atualizaPrecoTotal();
            st.setFloat(1, p.getPrecoTotal());
            st.setString(2, data);
            st.setInt(3, p.getIdPedido());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                for(ItemPedido ip : p.getItensPedido()){
                    ip.setPedido(p);
                    if(ip.getIdItem() == 0){
                        ipDAO.insertItemPedido(ip);
                    }
                    else if(ip.getSabor() == null){
                        ipDAO.deleteItemPedido(ip);
                    }
                    else{
                        ipDAO.updateItemPedido(ip);
                    }
                }
                
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean deletePedido(Pedido p) {
        PreparedStatement st = null;
        if(deleteItensPedido(p)){
            try {
                st = con.prepareStatement("DELETE FROM PEDIDO WHERE ID_PEDIDO = ?;");
                st.setInt(1, p.getIdPedido());
                int rowsAffected = st.executeUpdate();
                if(rowsAffected != 0){
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                if (st != null) {
                    try {
                        st.close(); } catch (Exception ex) {
                    }
                }
            }
        }
        return false;
    }
    public boolean deleteItensPedido(Pedido pedido) {
        PreparedStatement st = null;
        ItemPedidoDAOImpl itemPedidoDAO = new ItemPedidoDAOImpl(con);
        List<ItemPedido> listaItensPedido = itemPedidoDAO.selectItensPedido(pedido);
        for(ItemPedido p : listaItensPedido){
            if(!itemPedidoDAO.deleteItemPedido(p)){
                return false;
            }
        }
        return true;
    }
    
    public List<Pedido> selectPedidosCliente(Cliente c){
        ArrayList<Pedido> lista = new ArrayList<Pedido>();
        ItemPedidoDAOImpl itemPedidoDAO = new ItemPedidoDAOImpl();
        PreparedStatement st = null;
        ResultSet rs = null;
        Pedido aux;
        try{
            st = con.prepareStatement("SELECT P.ID_PEDIDO, P.ESTADO, P.DATA, P.PRECOTOTAL FROM PEDIDO P WHERE P.ID_CLIENTE = ?;");
            st.setInt(1, c.getIdCliente());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Pedido();
                aux.setIdPedido(rs.getInt("P.ID_PEDIDO"));
                aux.setEstado(rs.getString("P.ESTADO"));    
                aux.setData(rs.getDate("P.DATA"));  
                aux.setPrecoTotal(rs.getFloat("P.PRECOTOTAL"));                
                aux.setCliente(c);
                aux.setItensPedido(itemPedidoDAO.selectItensPedido(aux));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return lista;
    }
    public Pedido selectUltimoPedido(){
        ItemPedidoDAOImpl itemPedidoDAO = new ItemPedidoDAOImpl();
        PreparedStatement st = null;
        ResultSet rs = null;
        Pedido aux = null;
        try{
            st = con.prepareStatement("SELECT P.ID_PEDIDO, P.ESTADO, P.DATA, P.PRECOTOTAL FROM PEDIDO P WHERE P.ID_PEDIDO = (SELECT MAX(ID_PEDIDO) FROM PEDIDO);");
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Pedido();
                aux.setIdPedido(rs.getInt("P.ID_PEDIDO"));
                aux.setEstado(rs.getString("P.ESTADO"));    
                aux.setData(rs.getDate("P.DATA"));
                aux.setPrecoTotal(rs.getFloat("P.PRECOTOTAL"));
                aux.setItensPedido(itemPedidoDAO.selectItensPedido(aux));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return aux;
    }
    public Pedido selectIdPedido(Pedido p){
        ItemPedidoDAOImpl itemPedidoDAO = new ItemPedidoDAOImpl();
        PreparedStatement st = null;
        ResultSet rs = null;
        Pedido aux = null;
        try{
            st = con.prepareStatement("SELECT P.ID_PEDIDO, P.ESTADO, P.DATA, P.PRECOTOTAL FROM PEDIDO P WHERE P.ID_PEDIDO = ?;");
            st.setInt(1, p.getIdPedido());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Pedido();
                aux.setIdPedido(rs.getInt("P.ID_PEDIDO"));
                aux.setEstado(rs.getString("P.ESTADO"));    
                aux.setData(rs.getDate("P.DATA"));
                aux.setPrecoTotal(rs.getFloat("P.PRECOTOTAL"));
                aux.setItensPedido(itemPedidoDAO.selectItensPedido(aux));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return aux;
    }

    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    
}
