
package Pizzaria;

import java.util.List;

public interface SaborDao {
   public boolean inserirSabor(Sabor s);
   public List<Sabor> buscaTodosSabores();
   public List<Sabor> filtrarSabores(String filtro);
}
