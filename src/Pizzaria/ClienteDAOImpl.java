/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAOImpl implements ClienteDAO{

    private Connection con;
    
    public ClienteDAOImpl(){
        try {
            this.con = ConnectionFactory.getConnection();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public boolean insertCliente(Cliente c) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("INSERT INTO CLIENTE(NOME,SOBRENOME,TELEFONE) VALUES( ?,?,?);");
            st.setString(1, c.getNome());
            st.setString(2, c.getSobreNome());
            st.setString(3, c.getTelefone());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean updateCliente(Cliente c) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("UPDATE CLIENTE C SET C.NOME = ?, C.SOBRENOME = ?,C.TELEFONE = ? WHERE ID_CLIENTE = ?");
            st.setString(1, c.getNome());
            st.setString(2, c.getSobreNome());
            st.setString(3, c.getTelefone());
            st.setInt(4, c.getIdCliente());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean deleteCliente(Cliente c) {
        PreparedStatement st = null;
        PedidoDAOImpl pedidoDAO= new PedidoDAOImpl(con);
        if(deletePedidosCliente(c)){
            try {
                //deletar todos os pedidos e itemPedidosPrimeiro
                st = con.prepareStatement("DELETE FROM CLIENTE WHERE ID_CLIENTE = ?;");
                st.setInt(1, c.getIdCliente());
                int rowsAffected = st.executeUpdate();
                if(rowsAffected != 0){
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                if (st != null) {
                    try {
                        st.close(); } catch (Exception ex) {
                    }
                }
            }
        }

        return false;
    }
    public boolean deletePedidosCliente(Cliente c) {
        PreparedStatement st = null;
        PedidoDAOImpl pedidoDAO = new PedidoDAOImpl();
        List<Pedido> listaPedidos = pedidoDAO.selectPedidosCliente(c);
        for(Pedido p : listaPedidos){
            if(!pedidoDAO.deletePedido(p)){
                return false;
            }
        }
        return true;
    }
    public List<Cliente> buscaTodosClientes(){
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        PreparedStatement st = null;
        ResultSet rs = null;
        Cliente aux;
        try{
            st = con.prepareStatement("SELECT C.ID_CLIENTE, C.NOME, C.SOBRENOME, C.TELEFONE FROM CLIENTE C;");
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Cliente();
                aux.setIdCliente(rs.getInt("C.ID_CLIENTE"));
                aux.setNome(rs.getString("C.NOME"));    
                aux.setSobreNome(rs.getString("C.SOBRENOME"));
                aux.setTelefone(rs.getString("C.TELEFONE"));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return lista;
    }

    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
