package Pizzaria;

import java.util.Date;
import java.util.List;

public class Pedido {
    
    int idPedido;
    Cliente cliente;
    Date data;
    String estado;
    float precoTotal;
    List<ItemPedido> itensPedido;

    public List<ItemPedido> getItensPedido() {
        return itensPedido;
    }

    public void setItensPedido(List<ItemPedido> itensPedido) {
        this.itensPedido = itensPedido;
    }
   

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public float atualizaPrecoTotal() {
        float preco = 0;
        if(itensPedido !=null && !itensPedido.isEmpty()){
            for(ItemPedido ip : itensPedido){
                if(ip.getSabor() != null)
                    preco += ip.getPreco();
            }
            
            
        }
        precoTotal = preco;
        return preco;
    }

    public float getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(float precoTotal) {
        this.precoTotal = precoTotal;
    }
    
    
    
}
