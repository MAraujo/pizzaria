/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

public class Sabor {
    
    private int idSabor;
    private Tipo tipo;
    private String nome;
    private String descricao;
    
    public Sabor (){
        
    }
    
    public int getIdSabor() {
        return this.idSabor;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sabor other = (Sabor) obj;
        if (this.idSabor != other.idSabor) {
            return false;
        }
        return true;
    }

    public void setIdSabor(int idSabor) {
        this.idSabor = idSabor;
    }
    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    
    public Sabor (String nome, String descricao){
        this.nome=nome;
        this.descricao=descricao;
    }
    
    public Sabor (String nome, String descricao, int idSabor){
        this.nome=nome;
        this.descricao=descricao;
        this.idSabor=idSabor;
    }
    
    public void setNome (String nome){
        this.nome=nome;
    }
    
    public String getNome (){
        return this.nome;
    }
    
    public void setDescricao(String descricao){
        this.descricao=descricao;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
}
