/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemPedidoDAOImpl implements ItemPedidoDAO {
    private Connection con;
    
    public ItemPedidoDAOImpl(){
         try {
             this.con = ConnectionFactory.getConnection();
         } catch (ClassNotFoundException ex) {
             ex.printStackTrace();
         } catch (SQLException ex) {
             ex.printStackTrace();
         }
        
    }
    public ItemPedidoDAOImpl(Connection con){
        this.con = con;
    }
    
    public boolean deleteItemPedido(ItemPedido itemPedido){
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("DELETE FROM ITENS_PEDIDO WHERE ID_ITEM = ?;");
            st.setInt(1, itemPedido.getIdItem());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }
        
        return false;
    }
    
    public List<ItemPedido> selectItensPedido(Pedido p){
        ArrayList<ItemPedido> lista = new ArrayList<ItemPedido>();
        SaborDaoImpl saborDAO = new SaborDaoImpl(con);
        FormaDAOImpl formaDAO = new FormaDAOImpl(con);
        PreparedStatement st = null;
        ResultSet rs = null;
        ItemPedido aux;
        try{
            st = con.prepareStatement("SELECT ID_ITEM, ID_FORMA,ID_SABOR,ID_SABOR2,TAMANHO,QUANTIDADE, OBSERVACAO, PRECO FROM ITENS_PEDIDO IP WHERE IP.ID_PEDIDO = ?;");
            st.setInt(1, p.getIdPedido());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new ItemPedido();
                aux.setIdItem(rs.getInt("ID_ITEM"));
                aux.setForma(formaDAO.selectIDForma(rs.getInt("ID_FORMA")));
                aux.setSabor(saborDAO.buscaSabor(rs.getInt("ID_SABOR")));                
                aux.setSabor2(saborDAO.buscaSabor(rs.getInt("ID_SABOR2")));
                aux.setTamanho(rs.getInt("TAMANHO"));
                aux.setQtd(rs.getInt("QUANTIDADE"));
                aux.setObs(rs.getString("OBSERVACAO"));
                aux.setPreco(rs.getFloat("PRECO"));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return lista;
    }

    public boolean insertItemPedido(ItemPedido ip) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("INSERT INTO ITENS_PEDIDO"+
                    "(ID_PEDIDO,ID_FORMA,ID_SABOR,ID_SABOR2,TAMANHO,QUANTIDADE,OBSERVACAO,PRECO)"+
                    "VALUES(?,?,?,?,?,?,?,?);");
            st.setInt(1, ip.getPedido().getIdPedido());
            st.setInt(2, ip.getForma().getIdForma());
            st.setInt(3, ip.getSabor().getIdSabor());
            if(ip.getSabor2()!= null){
                st.setInt(4, ip.getSabor2().getIdSabor());
            }
            else{
                st.setInt(4, 0);
            }
            st.setInt(5, ip.getTamanho());
            st.setFloat(6, ip.getQtd());
            st.setString(7,ip.getObs());
            st.setFloat(8, ip.getPreco());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    
    }
    public boolean updateItemPedido(ItemPedido ip) {
       
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("UPDATE ITENS_PEDIDO IP SET "+
                    "IP.ID_FORMA = ?,IP.ID_SABOR = ?,IP.ID_SABOR2 = ?,IP.TAMANHO = ?,IP.QUANTIDADE = ?,IP.OBSERVACAO = ?,IP.PRECO = ? "+
                    "WHERE ID_ITEM = ?");
            st.setInt(1, ip.getForma().getIdForma());
            st.setInt(2, ip.getSabor().getIdSabor());
            if(ip.getSabor2()!= null){
                st.setInt(3, ip.getSabor2().getIdSabor());
            }
            else{
                st.setNull(3, 0);
            }
            st.setInt(4, ip.getTamanho());
            st.setInt(5, ip.getQtd());
            st.setString(6," ");
            if(ip.getObs() != null){
                st.setString(6,ip.getObs());
            }
            st.setFloat(7, ip.getPreco());
            st.setInt(8, ip.getIdItem());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    
    }
    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
