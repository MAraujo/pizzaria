/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FormaDAOImpl implements FormaDAO {
    private Connection con;
    
    public FormaDAOImpl(){
         try {
             this.con = ConnectionFactory.getConnection();
         } catch (ClassNotFoundException ex) {
             ex.printStackTrace();
         } catch (SQLException ex) {
             ex.printStackTrace();
         }
        
    }
    public FormaDAOImpl(Connection con){
        this.con = con;
    }
    
    public Forma selectIDForma(int idForma){
        Forma forma = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = con.prepareStatement("SELECT ID_FORMA,DESCRICAO FROM FORMA F WHERE F.ID_FORMA = ?;");
            st.setInt(1, idForma);
            rs = st.executeQuery();
            while(rs.next()){
                switch(rs.getString("DESCRICAO")){
                    case "Quadrado" :
                        forma = new Quadrado();
                        break;
                    case "Triangulo" :
                        forma = new Triangulo();
                        break;
                    case "Circulo" :
                        forma = new Circulo();
                        break;
                }
                forma.setIdForma(rs.getInt("ID_FORMA"));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return forma;
    }
    
    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
