/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.util.List;

/**
 *
 * @author ArtVin
 */
public interface TipoDAO {
    public List<Tipo> buscaTodosTipo();
    public boolean deleteTipo(Tipo t);
    public boolean insertTipo(Tipo t);
    public Tipo selectTipoID(int idTipo);
    public boolean updateTipo(Tipo t);
    
}
