/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.util.List;

/**
 *
 * @author ArtVin
 */
public interface ClienteDAO {
    public boolean insertCliente(Cliente c);
    
    public boolean updateCliente(Cliente c);
    
    public boolean deleteCliente(Cliente c);
    
    public List<Cliente> buscaTodosClientes();
    
    public void close();
}
