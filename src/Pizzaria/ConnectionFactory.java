/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static final int MYSQL = 0;
    public static final int POSTGRES = 1;
    private static final String MySQLDriver = "com.mysql.jdbc.Driver";
    private static final String PostgresDriver = "com.postgres.Driver";

    public static Connection getConnection(
            String url, String nome, String senha, int banco)
            throws ClassNotFoundException, SQLException {

        switch (banco) {
            case MYSQL:

                Class.forName(MySQLDriver);

                break;
        }
        return DriverManager.getConnection(url, nome, senha);
    }

    public static Connection getConnection()
            throws ClassNotFoundException, SQLException {

        Class.forName(MySQLDriver);

        return DriverManager.getConnection("jdbc:mysql://localhost:3306/pizzaria", "root", "");
    }
}
