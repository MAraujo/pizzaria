
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SaborDaoImpl{
    private Connection con;

    public SaborDaoImpl(){
        try{
            this.con = ConnectionFactory.getConnection();
        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public SaborDaoImpl(Connection con){
        this.con = con;
    }
        
    public boolean inserirSabor (Sabor s){
       PreparedStatement st = null;
       try {
           st = con.prepareStatement ("INSERT INTO SABOR (NOME, DESCRICAO,ID_TIPO) VALUES (?,?,?);");
           st.setString(1, s.getNome());
           st.setString(2, s.getDescricao());
           st.setInt(3,s.getTipo().getIdTipo());
           int rowsAffected = st.executeUpdate();
           if(rowsAffected != 0){
                return true;
           }
       }catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
       }
       return false;
    } 
    
    public boolean removerSabor (Sabor s){
       PreparedStatement st = null;
       try {
           st = con.prepareStatement ("DELETE FROM SABOR WHERE ID_SABOR=?;");
           st.setInt(1, s.getIdSabor());
           int rowsAffected = st.executeUpdate();
           if(rowsAffected != 0){
                return true;
           }
       }catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
       }
       return false;
    } 
    
    public Sabor buscaSabor(int idSabor){
        PreparedStatement st = null;
        TipoDAOImpl tipoDAO = new TipoDAOImpl(con);
        ResultSet rs = null;
        Sabor aux = null;
        try{
            st = con.prepareStatement("select id_sabor, nome, descricao, id_tipo from sabor where id_sabor = ?;");
            st.setInt(1, idSabor);
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Sabor();
                aux.setIdSabor(rs.getInt("id_sabor"));
                aux.setNome(rs.getString("NOME"));    
                aux.setDescricao(rs.getString("descricao"));
                aux.setTipo(tipoDAO.selectTipoID(rs.getInt("id_tipo")));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        return aux;
    }
    
    public List<Sabor> buscaTodosSabores(){
        ArrayList<Sabor> lista = new ArrayList<Sabor>();
        TipoDAOImpl tipoDAO = new TipoDAOImpl(con);
        PreparedStatement st = null;
        ResultSet rs = null;
        Sabor aux;
        try{
            st = con.prepareStatement("select id_sabor, nome, descricao, id_tipo from sabor;");
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Sabor();
                aux.setIdSabor(rs.getInt("id_sabor"));
                aux.setNome(rs.getString("NOME"));    
                aux.setDescricao(rs.getString("descricao"));
                aux.setTipo(tipoDAO.selectTipoID(rs.getInt("id_tipo")));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        return lista;
    }
    public Sabor buscaSabor(String nome){
        PreparedStatement st = null;
        TipoDAOImpl tipoDAO = new TipoDAOImpl(con);
        ResultSet rs = null;
        Sabor aux = null;
        try{
            st = con.prepareStatement("select id_sabor, nome, descricao, id_tipo from sabor where nome like ?;");
            st.setString(1, nome);
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Sabor();
                aux.setIdSabor(rs.getInt("id_sabor"));
                aux.setNome(rs.getString("NOME"));    
                aux.setDescricao(rs.getString("descricao"));
                aux.setTipo(tipoDAO.selectTipoID(rs.getInt("id_tipo")));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        return aux;
    }
    public List<Sabor> filtrarSabor(String nome){
        ArrayList<Sabor> lista = new ArrayList<Sabor>();
        TipoDAOImpl tipoDAO = new TipoDAOImpl(con);
        PreparedStatement st = null;
        ResultSet rs = null;
        Sabor aux;
        try{
            st = con.prepareStatement("select id_sabor, nome, descricao, id_tipo from sabor where nome like ?;");
            st.setString(1, nome + "%");
            rs = st.executeQuery();
            
            while(rs.next()){
                aux = new Sabor();
                aux.setIdSabor(rs.getInt("id_sabor"));
                aux.setNome(rs.getString("nome"));    
                aux.setDescricao(rs.getString("descricao"));
                aux.setTipo(tipoDAO.selectTipoID(rs.getInt("id_tipo")));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        return lista;
    }
    
    public boolean alterarSabor (Sabor s){
       PreparedStatement st = null;
       try {
           st = con.prepareStatement ("UPDATE SABOR SET nome = ?, descricao = ?, id_tipo = ? WHERE ID_SABOR = ? ;");
           st.setString(1, s.getNome());
           st.setString(2, s.getDescricao());
           st.setInt(3, s.getTipo().getIdTipo());
           st.setInt(4, s.getIdSabor());
           int rowsAffected = st.executeUpdate();
           if(rowsAffected != 0){
                return true;
           }
       }catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
       }
       return false;
    }
    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
            
}
