/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TipoDAOImpl implements TipoDAO {
    private Connection con;
    
    public TipoDAOImpl(){
        try {
            this.con = ConnectionFactory.getConnection();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public TipoDAOImpl(Connection con){
        this.con = con;
    }

    public boolean insertTipo(Tipo t) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("INSERT INTO TIPO(NOME_TIPO,PRECO_CM2) VALUES(?,?);");
            st.setString(1, t.getNome());
            st.setFloat(2, t.getPreco());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean updateTipo(Tipo t) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("UPDATE TIPO T SET NOME_TIPO = ?,PRECO_CM2 = ? WHERE ID_TIPO = ?");
            st.setString(1, t.getNome());
            st.setFloat(2, t.getPreco());
            st.setInt(3, t.getIdTipo());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }

        return false;
    }
    
    public boolean deleteTipo(Tipo t) {
        PreparedStatement st = null;
        try {
            //deletar todos os pedidos e itemPedidosPrimeiro
            st = con.prepareStatement("DELETE FROM TIPO WHERE ID_TIPO = ?;");
            st.setInt(1, t.getIdTipo());
            int rowsAffected = st.executeUpdate();
            if(rowsAffected != 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
        }
       
        return false;
    }
   
    public List<Tipo> buscaTodosTipo(){
        ArrayList<Tipo> lista = new ArrayList<Tipo>();
        PreparedStatement st = null;
        ResultSet rs = null;
        Tipo aux;
        try{
            st = con.prepareStatement("SELECT T.ID_TIPO, T.NOME_TIPO, T.PRECO_CM2 FROM TIPO T;");
            rs = st.executeQuery();
            while(rs.next()){
                aux = new Tipo();
                aux.setIdTipo(rs.getInt("T.ID_TIPO"));
                aux.setNome(rs.getString("T.NOME_TIPO"));    
                aux.setPreco(rs.getFloat("T.PRECO_CM2"));
                lista.add(aux);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return lista;
    }
    public Tipo selectTipoID(int idTipo){
        PreparedStatement st = null;
        ResultSet rs = null;
        Tipo tipo = null;
        try{
            st = con.prepareStatement("SELECT T.ID_TIPO, T.NOME_TIPO, T.PRECO_CM2 FROM TIPO T WHERE ID_TIPO = ?;");
            st.setInt(1, idTipo);
            rs = st.executeQuery();
            while(rs.next()){
                tipo = new Tipo();
                tipo.setIdTipo(rs.getInt("T.ID_TIPO"));
                tipo.setNome(rs.getString("T.NOME_TIPO"));    
                tipo.setPreco(rs.getFloat("T.PRECO_CM2"));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if (st != null) {
                try {
                    st.close(); } catch (Exception ex) {
                }
            }
            if (rs != null) {
                try {
                    rs.close(); } catch (Exception ex) {
                }
            }
        }
        
        
        
        
        return tipo;
    }

    public void close(){
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
