/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

public class Circulo extends Forma{

    private final float pi = (float) 3.14;
    
    public Circulo(){
        super.setIdForma(2);
        super.setDescricao("Circulo");
        super.setValorMax(23);
        super.setValorMin(7);
    }
   
    public float area(int tamanho) {
        return (float) (Math.pow(tamanho,2) * pi);
    }
    public int tamanho(float area){
        return (int) Math.round(Math.sqrt(area/pi));
    }
    
}