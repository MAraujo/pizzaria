/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.util.List;

/**
 *
 * @author ArtVin
 */
public interface ItemPedidoDAO {
    public boolean deleteItemPedido(ItemPedido itemPedido);
    public boolean insertItemPedido(ItemPedido ip);
    public boolean updateItemPedido(ItemPedido ip);
    public List<ItemPedido> selectItensPedido(Pedido p);
}
