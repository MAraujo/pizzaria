/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

public class Quadrado extends Forma{

    public Quadrado(){
        super.setIdForma(1);
        super.setDescricao("Quadrado");
        super.setValorMax(40);
        super.setValorMin(10);
    }
    
    public float area(int tamanho) {
        return tamanho * tamanho;
    }
    public int tamanho(float area){
        return (int) Math.round(Math.sqrt(area));
    }
    
}
