/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizzaria;

import java.util.List;

/**
 *
 * @author ArtVin
 */
public interface PedidoDAO {
    public boolean deletePedido(Pedido p);
    public boolean insertPedido(Pedido p);
    public Pedido selectIdPedido(Pedido p);
    public List<Pedido> selectPedidosCliente(Cliente c);
    public Pedido selectUltimoPedido();
    public boolean updatePedido(Pedido p);
    public boolean updatePedidoEstado(Pedido p);
}
