/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizza;

/**
 *
 * @author Usuario
 */
public class Triangulo extends Forma {
    private double areaTriangulo;
    private double ladoTriangulo;
    
    @Override
    public double area(double lado){
        this.areaTriangulo=lado*(lado*Math.sqrt(3)/2)/2;
        return areaTriangulo;
    }
}
