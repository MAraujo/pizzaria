/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizza;

/**
 *
 * @author Usuario
 */
public class Quadrado extends Forma{
    private double areaQuadrado;
    private double ladoQuadrado;
    
    @Override
    public double area(double lado){
        areaQuadrado=Math.pow(lado, 2);
        return areaQuadrado;
    }
    
    public double getLadoQuadrado(){
        return ladoQuadrado;
    }
    
    public void setLadoQuadrado(double ladoQuadrado){
        this.ladoQuadrado=ladoQuadrado;
    }
    
    public double getAreaQuadrado(){
        return areaQuadrado;
    }
    public void setAreaQuadrado(double areaQuadrado){
        this.areaQuadrado=areaQuadrado;
    }
}
