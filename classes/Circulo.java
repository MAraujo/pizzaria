/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pizza;
/**
 *
 * @author Usuario
 */
public class Circulo extends Forma {
    private double areaCirculo;
    private double raio;
    
    @Override
    public double area(double raio){
        areaCirculo=Math.PI*Math.pow(raio, 2);
        return areaCirculo;
    }
}
